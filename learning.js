'use strict';

//RELEARNING JAVASCRIPT + TRYING TO COMPLETE FRONTEND FOR MUTAGEN BACKEND

let messsage;
message = 'Hello';
alert(message);

//study scala or erlang to experience single-time declaration of variables... ";..;"

const MY_BIRTHDAY = '133737'; //unchangeable variable, USE_UPPER_CASE

let iDono = null;           //represents “nothing”, “empty” or “value unknown”

let youDono;                //shows "undefined"
let youDid = undefined;      //same shit

typeof youDid;
typeof(youDono);

alert(MY_BIRTHDAY);
prompt(MY_BIRTHDAY);

let question = 'You big?';

let result = confirm(question); //if yes, result=true, false if no

//type conversion (casting)
let toString = String(result); //toString
let toNum = Number(result); //toNumber
let toBool = Boolean(question); //toBoolean



if(toString == toBool) {alert('wtf dude')};

let condition1 = (youdid == youDono);

//ternary operator ? : 
let result = condition ? value1 : value2;

//nested terniarys, not sure if I like this way of doing it 
let age = prompt('age?', 18);

let message = (age < 3) ? 'Hi, baby!' :
  (age < 18) ? 'Hello!' :
  (age < 100) ? 'Greetings!' :
  'What an unusual age!';

alert( message );

a ?? b; //returns the first argument(A) if it's not null/undefined, otherwise (B) used to provide a default value for a potentially undefined variable
resultLong = (a !== null && a !== undefined) ? a : b;
resultShort = a ?? b;

//functionTime, start their names with a verb (show, get, create, check, )

function showMessage() {
  alert( 'Hello everyone!' );
}
showMessage();




//way of always making sure you get whatever default value you want for param
function showMessage(text) {
  text = text || 'empty';
}

//better way of doing the same... kind of
function showCount(count) {
  alert(count ?? "unknown");
}

showCount(0); // 0
showCount(null); // unknown
showCount(); // unknown

//function Expressions...
function sayHi() {   // (1) create
  alert( "Hello" );
}

let func = sayHi;    // (2) copy

func(); // Hello     // (3) run the copy (it works)!
sayHi(); // Hello    //     this still works too (why wouldn't it)

function ask(question, yes, no) {
  if (confirm(question)) yes()
  else no();
}

function showOk() {
  alert( "You agreed." );
}

function showCancel() {
  alert( "You canceled the execution." );
}

// usage: functions showOk, showCancel are passed as arguments to ask, 'callbacks'
ask("Do you agree?", showOk, showCancel);

//WTF IS THIS
function ask(question, yes, no) {
  if (confirm(question)) yes()
  else no();
}

ask(
  "Do you agree?", function() { alert("You agreed."); }, function() { alert("You canceled the execution."); }
);

/*Regular values like strings or numbers represent the data.
A function can be perceived as an action.
We can pass it between variables and run when we want.*/

let func = function(arg1, arg2, argN) {
  return expression;
};

let sum = (a, b) => a + b;

let double = n => n*2;


//MAKE THIS WORK TO LEARN >: I DID IT
let potato = {
  size: 3,
  color: 'brown',
  dna : {
    sequence : ['A','C','G','T','T','A','A','C','G','T','A']
  }
}
console.log(Object.values(potato));
console.log(potato.dna.sequence.length);

//expected output of above loop is the array in potato, this is potato programming!
for(i = 0; i < potato.dna.sequence.length; i++){
console.log(potato.dna.sequence[i])
}

//THIS IS A MESS BUT OK LETS GET KEYS N SHIT
for(key in potato.dna.sequence){
  console.log(potato.dna.sequence[key])
}

//function used to set object's kind of fruit
let fruit = prompt("Which fruit to buy?", "apple");

let bag = {
  [fruit]: 5, // the name of the property is taken from the variable fruit
};

alert( bag.apple ); // 5 if fruit="apple"

// '?.' annotation, a bit weird but solves some checking issues, stops evaluating if value before ?. does not exist
let user = null;
alert( user?.address ); // undefined
alert( user?.address.street ); // undefined

//This is how far I got today! End!
let endOfDay = 'https://javascript.info/data-types';

JSON.stringify(objectToStringify); //converts java object to JSON string
JSON.parse(JSONToObjectify); //converts the JSON string to an object



/*Common JSON mistakes
let json = `{
  name: "John",                     // mistake: property name without quotes
  "surname": 'Smith',               // mistake: single quotes in value (must be double)
  'isAdmin': false                  // mistake: single quotes in key (must be double)
  "birthday": new Date(2000, 2, 3), // mistake: no "new" is allowed, only bare values
  "friends": [0,1,2,3]              // here all fine
}`; */
/*
arr.push(...items) – adds items to the end,
arr.pop() – extracts an item from the end,
arr.shift() – extracts an item from the beginning,
arr.unshift(...items) – adds items to the beginning.

arr.splice() delete
arr.slice(start, end)
arr.indexOf, find, findIndex*/

//Filter
let users = [
  {id: 1, name: "John"},
  {id: 2, name: "Pete"},
  {id: 3, name: "Mary"}
];

// returns array of the first two users
let someUsers = users.filter(item => item.id < 3);

alert(someUsers.length); // 2

//map
let result = arr.map(function(item, index, array) {
  // returns the new value instead of item
});

//split
//arr.split

//Array.isArray(arrayToCheck)


/* MAP
new Map() – creates the map.
map.set(key, value) – stores the value by the key.
map.get(key) – returns the value by the key, undefined if key doesn’t exist in map.
map.has(key) – returns true if the key exists, false otherwise.
map.delete(key) – removes the value by the key.
map.clear() – removes everything from the map.
map.size – returns the current element count. */

//CLASSES HOW DO

class MyClass {
  // class methods
  constructor() { }
//  method1() { ... }
//  method2() { ... }
//  method3() { ... }
  
}

/* Example JSON string from Mutagen
{"coordinate":{"x":0.0,"y":0.0},
"environment":"Marshlands","ruin":null,"mood":"Movie Poster. A torn and worn old poster from the Old Age shows a strange scene. What do the PCs make of it?",
"threat":["Acid Grass","description X","attributes A B C","skills A B","armor","weapons A B"],
"artifact":null,"scraps":["Football","VHS cassette","Jack","Telephone","Hot dog cart"]} 

{"coordinate":{"x":0.0,"y":0.0},"environment":"Marshlands","ruin":null,"mood":"Movie Poster. A torn and worn old poster from the Old Age shows a strange scene. What do the PCs make of it?","threat":["Acid Grass","description X","attributes A B C","skills A B","armor","weapons A B"],"artifact":null,"scraps":["Football","VHS cassette","Jack","Telephone","Hot dog cart"]}
*/




































/*
window.onload=function(){
document.getElementById("zoneCreationButton").addEventListener('click', createZone);
}

async function createZone(){
    const response = await fetch('http://localhost:8080/zone');
    const zone = await response.json(); //extract JSON from the http response
      return zone;
  }

 const createZone = async () => { //constant(const) reference to (var)createZone 
    const response = await fetch('http://localhost:8080/zone?');
    const myJson = await response.json(); //extract JSON from the http response
    // do something with myJson
  } */



