'use strict';
/* Example JSON string from Mutagen
{"coordinate":{"x":0.0,"y":0.0},
"environment":"Marshlands","ruin":null,"mood":"Movie Poster. A torn and worn old poster from the Old Age shows a strange scene. What do the PCs make of it?",
"threat":["Acid Grass","description X","attributes A B C","skills A B","armor","weapons A B"],
"artifact":null,"scraps":["Football","VHS cassette","Jack","Telephone","Hot dog cart"]} 

{"coordinate":{"x":0.0,"y":0.0},"environment":"Marshlands","ruin":null,"mood":"Movie Poster. A torn and worn old poster from the Old Age shows a strange scene. What do the PCs make of it?","threat":["Acid Grass","description X","attributes A B C","skills A B","armor","weapons A B"],"artifact":null,"scraps":["Football","VHS cassette","Jack","Telephone","Hot dog cart"]}
*/

//TODO: 
//Make static site look according to idea
//decide how to persist information gathered from Mutagen
//execute http request to Mutagen application
//put recieved information into db

//MinimumViableProduct:
//static site has 1 button DONE
//button => fetch 1 sector
let localMutagenURL = "http://localhost:8080/api/v1/sector";
let json;
function getSector(){
    let response = fetch(localMutagenURL, {mode: 'no-cors'})
    //let json = response.json();    
    /*if (response.ok) { // if HTTP-status is 200-299
            // get the response body (the method explained below)
           
          } else {
              console.log(response.then( response=> response.json));
            alert("HTTP-Error: " + response.status);
          }*/

    return response;
}
//alert(result of button press)

//Evolution of MVP
//fetch a complete zone
//save/persist zone in db
//represent zone in a grid on site (30x20)




//NOTES FOR CORS
/* same-origin policy

Under the hood, the browser checks if the origins of the web application
and the server match. Above, the origins were simplified to the frontend 
application and backend server domains. But really, the origin is the 
combination of the protocol, host, and port. For example, 
in https://www,facebook-clone.com, the protocol is https://, 
the host is www.facebook-clone.com, and the hidden port number is 443 
(the port number typically used for https).

Access-Control-Allow-Origin: *
Once the browser receives this header information back, 
it compares the frontend domain with the Access-Control-Allow-Origin 
value from the server. If the frontend domain does not match the value, 
the browser raises the red flag and blocks the API 
request with the CORS policy error.

Build your own proxy!
https://medium.com/@dtkatz/3-ways-to-fix-the-cors-error-and-how-access-control-allow-origin-works-d97d55946d9

*/